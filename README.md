# Annotate texts with background information while preserving reading flow

[![pipeline status](https://gitlab.com/georg.jung/bsc-thesis/badges/master/pipeline.svg)](https://gitlab.com/georg.jung/bsc-thesis/commits/master)

This repository contains the written part of Georg Jung's Bachelor's Thesis "Annotate texts with background information while preserving reading flow" (in german). It was submitted on 2019-07-10 and graded 1.0 (A+ equivalent). The corresponding application is hosted [on GitHub](https://github.com/georg-jung/PdfAnnotator). This repository serves as a real world example for using the [tudarmstadt-design-latex](https://gitlab.com/georg.jung/tudarmstadt-design-latex) repository too.

## Latest builds

* **[PDF](https://gitlab.com/georg.jung/bsc-thesis/-/jobs/artifacts/master/raw/compile/thesis.pdf?job=install-tudesign-and-compile)** - the final document as submitted
* [HTML](https://gitlab.com/georg.jung/bsc-thesis/-/jobs/artifacts/master/raw/converted/thesis.html?job=create-other-formats) - for easy viewing in the browser and on a smartphone
* [DOCX](https://gitlab.com/georg.jung/bsc-thesis/-/jobs/artifacts/master/raw/converted/thesis.docx?job=create-other-formats) - to use Microsoft Word for grammar and spell checking
* [Software PdfAnnotator via GitHub](https://github.com/georg-jung/PdfAnnotator/releases/latest/download/pdfannotator.zip)
  * See the [repository](https://github.com/georg-jung/PdfAnnotator)

## Based on tudarmstadt-design-latex

See <https://gitlab.com/georg.jung/tudarmstadt-design-latex>.

## License information

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Bitte beachten Sie, dass das Corporate Design der TU Darmstadt genauso wie auch das hier verwendete Logo der TU Darmstadt nur nach den Regeln des jeweiligen Rechteinhabers verwendet werden dürfen. Insbesondere stehen die entsprechenden Inhalte nicht unter der Creative Commons-Lizenz, die für das restliche Projekt gilt. Weitere Informationen finden Sie auf den [Seiten der TU Darmstadt zum Thema](https://www.intern.tu-darmstadt.de/arbeitsmittel/corporate_design_vorlagen/index.de.jsp).
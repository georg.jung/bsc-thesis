#!/bin/sh

# Create Corporate Design install working dir
mkdir src/tudesign-unpacked

# extract original files in working dir
unzip -q -o 'src/corporatedesign/*.zip' -d src/tudesign-unpacked/

# fix working dir
sh src/scripts/helper/installstep-fix-affidavit.sh src/tudesign-unpacked
sh src/scripts/helper/installstep-fix-a1b-compliance.sh src/tudesign-unpacked

# install from working dir to local texmf
sh src/scripts/helper/installstep-texmf.sh src/tudesign-unpacked
echo "Successfully installed TU Darmstadt Corporate Design."
